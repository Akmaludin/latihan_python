sepatu = {"nama":"Sepatu Niko", "harga":150000, "diskon":30000}
baju = {"nama":"Baju Unikloh", "harga":80000, "diskon":8000}
celana = {"nama":"Celana Lepis", "harga":200000, "diskon":60000}
jam_tangan = {"nama":"Jam Tangan", "harga":150000, "diskon":20000}
tas = {"nama":"Tas", "harga":250000, "diskon":10000}

# Pajak 10% dari nilai jual

harga_sepatu = sepatu["harga"] - sepatu["diskon"]
harga_baju = baju["harga"] - baju["diskon"]
harga_celana = celana["harga"] - celana["diskon"]
harga_jam_tangan = jam_tangan["harga"] - jam_tangan["diskon"]
harga_tas = tas["harga"] - baju["diskon"]

total_harga = harga_sepatu + harga_baju + harga_celana + harga_jam_tangan + harga_tas

# Total harga setelah dikalikan pajaknya
total_akhir = (total_harga * 0.1) + total_harga
print(total_akhir)