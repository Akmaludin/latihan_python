import pandas as pd

latihan1 = pd.read_excel("Dataset_Transaksi.xlsx")
print(latihan1.mean())
print("Dataset yang masih kosong!:")
print(latihan1.head(20))  

latihan1 = latihan1.fillna(latihan1.mean())
print("Dataset yang sudah diproses Handling Missing Values dengan Mean :")
print(latihan1.head(20))