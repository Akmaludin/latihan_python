'''
Satuannya dalam bentuk jutaan
Berikut ini merupakan total pengeluaran dan pemasukan saya :
'''

# Data Keuangan
keuangan = {
'pengeluaran' : [2, 2.5, 3, 1, 1.5, 2, 4, 2.4, 2.5, 3, 1.5, 2],
'pemasukan' : [6, 8, 10, 8, 9, 10, 8, 9, 10, 20, 15, 25]   
}

#Perhitungan rata-rata pemasukan dan rata-rata pengeluaran
total_pengeluaran = 0
total_pemasukan = 0

for biaya in keuangan['pengeluaran']:
    total_pengeluaran += biaya
for biaya in keuangan['pemasukan']:
    total_pemasukan += biaya
rata_rata_pengeluaran = total_pengeluaran/len(keuangan['pengeluaran'])
rata_rata_pemasukan = total_pemasukan/len(keuangan['pemasukan'])
print(rata_rata_pengeluaran.__round__(2))
print(rata_rata_pemasukan.__round__(2))